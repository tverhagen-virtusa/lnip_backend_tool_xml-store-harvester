package com.lexisnexis.lnip.filestore.vertx;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;

import org.apache.commons.io.IOUtils;
import org.infinispan.commons.dataconversion.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.UniventioFileStore;
import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;
import com.lexisnexis.lnip.importer.config.TransformConfig;
import com.lexisnexis.lnip.importer.config.TransformConfig.TransformationName;
import com.lexisnexis.lnip.importer.services.TransformationService;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

public class Translator extends AbstractVerticle {
    private static Logger logger = LoggerFactory.getLogger(Translator.class);
    private UniventioFileStore xmlFileStore;
    private UniventioFileStore jsonFileStore;

    private final Map<MediaType, String> mediaTypeToExtension = new HashMap<>();

    private TransformConfig transformConfig = new TransformConfig();
    private TransformationService transformationService;


    public Translator() {
        mediaTypeToExtension.put(MediaType.APPLICATION_XML, "xml");
        mediaTypeToExtension.put(MediaType.APPLICATION_JSON, "json");
    }


    @Override
    public void start(Promise<Void> promise) throws IOException {

        Path path = Paths.get(".");
        if (Files.exists(path)) {
            logger.info("Current path {}", path.toAbsolutePath());
        }
        try {
            transformationService = new TransformationService(transformConfig.jaxbContext(), transformConfig.getTemplates());
            promise.complete();
        }
        catch (TransformerConfigurationException | JAXBException e) {
            promise.fail(e);
        }

        initialiseXmlStore();
        initialiseJsonStore();

        EventBus bus = vertx.eventBus();
        bus.<JsonObject>consumer("file-store.translate.xml-to-json", msg -> {
            JsonObject body = msg.body();
            String id = body.getString("id");
            Publication publication = Publication.create(body.getString("publication-id"));
            logger.info("{} translate for publication-id {}", id, publication);
            Path file = xmlFileStore.getPath(publication, "xml");
            logger.info("Input {}", file);

            try {
                jsonFileStore.createPath(publication);
                Path outputFile = jsonFileStore.getPath(publication, "json");
                logger.info("Output (fileStore) {}", outputFile);

                Path newFilename = replaceExtension(file, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML);
                logger.info("Output {}", newFilename);

                InputStream is;
                is = new FileInputStream(file.toFile());
                ByteArrayOutputStream os = transformationService.transform(TransformationName.DISPLAY, IOUtils.toByteArray(is));
                try (OutputStream outputStream = new FileOutputStream(Files.createFile(outputFile).toFile())) {
                   os.writeTo(outputStream);
                }
            }
            catch (TransformerConfigurationException | IOException e) {
                logger.warn("Failure running translation '" + TransformationName.DISPLAY + "' for file '" + file + "'", e);
            }
        });

    }


    private void initialiseXmlStore() throws IOException {
        JsonObject fileStoreJson = config().getJsonObject("xml-file-store");
        logger.info("root-path {}", fileStoreJson.getString("root-path"));
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(
                null
                , null
                , fileStoreJson.getString("root-path")
                , fileStoreJson.getJsonArray("paths").getList()
                , fileStoreJson.getJsonArray("content-type").getList());
        xmlFileStore = new UniventioFileStore(fileStoreCfg);
    }
    private void initialiseJsonStore() throws IOException {
        JsonObject fileStoreJson = config().getJsonObject("json-file-store");
        logger.info("root-path {}", fileStoreJson.getString("root-path"));
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(
                fileStoreJson.getString("name")
                , fileStoreJson.getString("environment")
                , fileStoreJson.getString("root-path")
                , fileStoreJson.getJsonArray("paths").getList()
                , fileStoreJson.getJsonArray("content-type").getList());
        jsonFileStore = new UniventioFileStore(fileStoreCfg);
    }

    public Path replaceExtension(Path filename, MediaType extToAdd, MediaType... extToRemove) {
        return addExtension(removeExtension(filename, extToRemove), extToAdd);
    }

    public Path removeExtension(Path filename, MediaType... extToRemove) {
        if (filename.getFileName().toString().indexOf(".") < 0) {
            return filename;
        }

        Set<String> extensions = new HashSet<>();
        for (MediaType mediaType : extToRemove) {
            extensions.add(mediaTypeToExtension.get(mediaType));
        }
        return removeExtension(filename, extensions.toArray(new String[0]));
    }
    public Path removeExtension(Path filename, String... extToRemove) {
        if (filename.getFileName().toString().indexOf(".") < 0) {
            return filename;
        }

        List<String> extensions = Arrays.asList(extToRemove);
        String ext = filename.getFileName().toString().substring(filename.getFileName().toString().lastIndexOf('.') + 1);
        if (extensions.contains(ext)) {
            Path parent = filename.getParent();
            String fileWithoutExt = filename.getFileName().toString().substring(0, filename.getFileName().toString().lastIndexOf('.'));
            return (parent == null) ? Paths.get(fileWithoutExt) : parent.resolve(fileWithoutExt);
        }
        return filename;
    }

    public Path addExtension(Path filename, MediaType mediaType) {
        if (mediaType == null) {
            throw new IllegalArgumentException("Argument mediaType should not be null.");
        }
        if (! mediaTypeToExtension.containsKey(mediaType)) {
            throw new IllegalArgumentException("Argument 'mediaType' with value '" + mediaType + "' is not a know mediaType, for file extension.");
        }
        Path parent = filename.getParent();
        String fileWithExt = filename.getFileName().toString() + "." + mediaTypeToExtension.get(mediaType);
        return (parent == null) ? Paths.get(fileWithExt) : parent.resolve(fileWithExt);
    }
}
