package com.lexisnexis.lnip.filestore.vertx;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class FileStoreApp {

    public static void main(String[] args) {
        JsonObject fileStoreConfig = createFileStoreConfig();

        Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(Translator.class.getName(), new DeploymentOptions().setConfig(fileStoreConfig).setInstances(1));
        vertx.deployVerticle(GeneratePublicationIds.class.getName());
    }


    private static JsonObject createFileStoreConfig() {
        JsonObject json = new JsonObject()
                .put("xml-file-store", new JsonObject()
                        .put("name", "Xml File Store for test input")
//                        .put("content-type", new JsonArray().add("application/xml"))
                        .put("content-type", new JsonArray().add("xml"))
                        .put("root-path", "../lnip_tool_file-store/src/test/resources/data/xml-store")
                        .put("paths", new JsonArray()
                                .add("s1")
                                .add("s5")
                                .add("s6")
                        )
                )
                .put("json-file-store", new JsonObject()
                        .put("name", "Json File Store for test output")
                        .put("environment", "develop")
//              .put("content-type", new JsonArray().add("application/xml"))
                        .put("content-type", new JsonArray().add("json"))
                        .put("root-path", "target/json-store")
                        .put("paths", new JsonArray()
                                .add("drive-1/JO")
                        )
                );
        return json;
    }
}
