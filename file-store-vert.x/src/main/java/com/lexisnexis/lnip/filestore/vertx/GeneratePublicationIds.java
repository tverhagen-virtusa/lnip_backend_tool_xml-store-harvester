package com.lexisnexis.lnip.filestore.vertx;

import java.util.UUID;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

public class GeneratePublicationIds extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        vertx.setTimer(2000, h -> {
            JsonObject json = new JsonObject()
                    .put("id", UUID.randomUUID().toString())
                    .put("publication-id", "JO1035B");
            
            vertx.eventBus().publish("file-store.translate.xml-to-json", json);
        });
    }

}
