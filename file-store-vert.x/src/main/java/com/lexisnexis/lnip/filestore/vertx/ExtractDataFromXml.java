package com.lexisnexis.lnip.filestore.vertx;

import java.io.IOException;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.UniventioFileStore;
import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.file.AsyncFile;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.json.JsonObject;

public class ExtractDataFromXml extends AbstractVerticle {
    private static Logger logger = LoggerFactory.getLogger(ExtractDataFromXml.class);
    private UniventioFileStore xmlFileStore;


    @Override
    public void start(Promise<Void> promise) throws IOException {
        initialiseXmlStore();

        EventBus bus = vertx.eventBus();
        bus.<JsonObject>consumer("file-store.translate.xml-to-json", msg -> {
            JsonObject body = msg.body();
            String id = body.getString("id");
            Publication publication = Publication.create(body.getString("publication-id"));
            logger.info("{} translate for publication-id {}", id, publication);
            Path file = xmlFileStore.getPath(publication, "xml");
            logger.info("Input {}", file);
            
            vertx.fileSystem().open(file.toString(), new OpenOptions(), result -> {
                if (result.succeeded()) {
                  AsyncFile asyncFile = result.result();
//                  asyncFile.
//                  Buffer buff = Buffer.buffer(1000);
//                  for (int i = 0; i < 10; i++) {
//                    asyncFile.read(buff, i * 100, i * 100, 100, ar -> {
//                      if (ar.succeeded()) {
//                        System.out.println("Read ok!");
//                      } else {
//                        System.err.println("Failed to write: " + ar.cause());
//                      }
//                    });
//                  }
                } else {
                  System.err.println("Cannot open file " + result.cause());
                }
              });
        });
    }

    private void initialiseXmlStore() throws IOException {
        JsonObject fileStoreJson = config().getJsonObject("xml-file-store");
        logger.info("root-path {}", fileStoreJson.getString("root-path"));
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(
                null
                , null
                , fileStoreJson.getString("root-path")
                , fileStoreJson.getJsonArray("paths").getList()
                , fileStoreJson.getJsonArray("content-type").getList());
        xmlFileStore = new UniventioFileStore(fileStoreCfg);
    }

}
