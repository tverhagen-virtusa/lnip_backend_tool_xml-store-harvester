package com.lexisnexis.lnip.filestore.vertx;

import static org.junit.Assert.assertEquals;

import java.nio.file.Paths;

import org.infinispan.commons.dataconversion.MediaType;
import org.junit.Test;

public class TranslatorTest {
    private Translator translator = new Translator();


    @Test
    public void add() {
        assertEquals(Paths.get("some.other.json"), translator.addExtension(Paths.get("some.other"), MediaType.APPLICATION_JSON));
    }

    @Test
    public void remove() {
        assertEquals(Paths.get("some.other"), translator.removeExtension(Paths.get("some.other"), MediaType.APPLICATION_JSON));
    }

    @Test
    public void replaceExtension() {
        assertEquals(Paths.get("some.other.json"), translator.replaceExtension(Paths.get("some.other.xml"), MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML));

        assertEquals(Paths.get("document.pdf.xml"), translator.replaceExtension(Paths.get("document.pdf.json"), MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON));
    }

}
