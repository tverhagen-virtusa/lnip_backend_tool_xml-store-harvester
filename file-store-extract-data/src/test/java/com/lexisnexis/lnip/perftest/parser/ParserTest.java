package com.lexisnexis.lnip.perftest.parser;

import static org.junit.Assert.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;

public class ParserTest {
    private Path resourcesPath = Paths.get("src/test/resources");
    private Path targetPath = Paths.get("target");


    @Test
    @Ignore
    public void extractData() throws Exception {
        XmlMetaDataProcessor processor = new XmlMetaDataProcessor(new XmlTagsAndAttributesOfInterestSample());

        Path outputCsvFile = Paths.get("/home/gpcmol/Desktop/metadata.csv");
        Path dataPath = Paths.get("/home/gpcmol/Desktop/xmls");
        List<Path> files = processor.getFilenamesFromDirecory(dataPath);

        for (Path file : files) {
            processor.writeMetaDataToCsv(outputCsvFile, file);
        }
    }

    @Test
    public void extractDataRavi() {
        XmlMetaDataProcessor processor = new XmlMetaDataProcessor(new XmlTagsAndAttributesOfInterestSample());

        Path outputCsvFile = targetPath.resolve("metadata.csv");
        Path dataPath  = resourcesPath.resolve("data");
        List<Path> files = processor.getFilenamesFromDirecory(dataPath);

        for (Path file : files) {
            processor.writeMetaDataToCsv(outputCsvFile, file);
        }
    }


    
    static class Xww implements XmlTagsAndAttributesOfInterest {

        @Override
        public Map<String, List<String>> getPointsOfInterest() {
            Map<String, List<String>> poi = new LinkedHashMap<>();

            poi.put("univentio-patent-document.Publication", Arrays.asList("PublicationId", "Status"));
            poi.put("Publication.Date", null);
            poi.put("Publication.Kind", Arrays.asList("Type"));
            poi.put("Publication.Authority", Arrays.asList("Code"));
            poi.put("Raw.Number", null);
            poi.put("Raw.Kind", null);

            poi.put("MainFamily.FamilyId", null);
            poi.put("CompleteFamily.FamilyId", null);
            poi.put("DomesticFamily.FamilyId", null);
            poi.put("FullFamily.FamilyId", null);
            poi.put("SimpleFamily.FamilyId", null);

            poi.put("Titles.Title", Arrays.asList("Lang_ISO639-2", "DataType"));
            poi.put("Claims.Claims", Arrays.asList("Lang_ISO639-2", "DataType"));
            poi.put("Abstracts.Abstracts", Arrays.asList("Lang_ISO639-2", "DataType"));
            poi.put("Descriptions.Description", Arrays.asList("Lang_ISO639-2", "DataType"));
            
            return poi;
        }
        
    }
}
