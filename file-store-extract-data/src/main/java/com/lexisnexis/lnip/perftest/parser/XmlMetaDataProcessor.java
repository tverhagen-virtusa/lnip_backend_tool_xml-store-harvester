package com.lexisnexis.lnip.perftest.parser;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class XmlMetaDataProcessor {
    private static Logger log = LoggerFactory.getLogger(XmlMetaDataProcessor.class);
    private XmlTagsAndAttributesOfInterestSample infoOfInterest;


    public XmlMetaDataProcessor(XmlTagsAndAttributesOfInterestSample infoOfInterest) {
        if (infoOfInterest == null) {
            throw new IllegalArgumentException("Argument 'infoOfInterest' should not be null.");
        }
        this.infoOfInterest = infoOfInterest;
    }


    private long fileSizeBytes(Path file) throws IOException {
        FileChannel fileChannel = FileChannel.open(file);
        return fileChannel.size();
    }


    public void writeMetaDataToCsv(Path outputCsvFile, Path dataFile) {
        try {
            Map<String, List<String>> metaData = getMetaData(dataFile);
            Map.Entry<String, String> headerContentEntry = makeCsvLine(metaData).entrySet().iterator().next();
            writeToCsv(outputCsvFile, headerContentEntry.getKey() + "\n", headerContentEntry.getValue() + "\n");
            log.info(".");
        } catch (Exception e) {
            log.error("Error processing " + dataFile + ": " + e.getMessage());
        }
    }

    public List<Path> getFilenamesFromDirecory(Path directory) {
        try (Stream<Path> walk = Files.walk(directory)) {
            List<Path> result = walk.filter(Files::isRegularFile).collect(Collectors.toList()); //map(x -> x.toAbsolutePath()).
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Arrays.asList();
    }

    public void writeToCsv(Path csvFile, String header, String content) throws IOException {
        // write header once
        if (!Files.exists(csvFile)) {
            Files.write(csvFile, header.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        }
        Files.write(csvFile, content.getBytes(), StandardOpenOption.APPEND);
    }

    /* key=header, value=content */
    private Map<String, String> makeCsvLine(Map<String, List<String>> data) {
        // hacky fix. this field could miss
        List<String> headerValues = Arrays.asList("Meta.File", "Meta.Bytes", "univentio-patent-document.Publication.PublicationId", "univentio-patent-document.Publication.Status", "Publication.Authority.Code", "Raw.Number", "Raw.Kind", "DomesticFamily.FamilyId", "MainFamily.FamilyId", "CompleteFamily.FamilyId", "FullFamily.FamilyId", "SimpleFamily.FamilyId", "Titles.Title.Lang_ISO639-2", "Titles.Title.DataType", "Descriptions.Description.Lang_ISO639-2", "Descriptions.Description.DataType", "Claims.Claims.Lang_ISO639-2", "Claims.Claims.DataType");

        for (String headerValue : headerValues) {
            if (!data.keySet().contains(headerValue)) {
                data.put(headerValue, Arrays.asList("null"));
            }
        }

        data = data.entrySet().stream().sorted(comparingByKey()).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

        Map<String, String> headerContent = new HashMap<>();

        List<String> header = new ArrayList<>();
        List<String> content = new ArrayList<>();

        for (Map.Entry<String, List<String>> entry : data.entrySet()) {
            header.add(fixHeaderName(entry.getKey()));

            // only add the first value
            if (entry.getKey().startsWith("Raw") || entry.getKey().startsWith("Publication.Authority.Code")) {
                content.add(entry.getValue().get(0));
            } else {
                content.add(String.join(",", entry.getValue()));
            }

            //log.info(fixHeaderName(entry.getKey()) + " " + entry.getValue());
        }

        headerContent.put(String.join(";", header), String.join(";", content));

        return headerContent;
    }

    public Map<String, List<String>> getMetaData(Path xmlFile) throws Exception {
        Map<String, List<String>> results = new LinkedHashMap<>();
        log.info("Get meta data for '" + xmlFile + "'");
        long sizeInBytes = fileSizeBytes(xmlFile);

        results.put("Meta.File", Arrays.asList(xmlFile.toString()));
        results.put("Meta.Bytes", Arrays.asList(String.valueOf(sizeInBytes)));

        Map<String, List<String>> elementAttributes = infoOfInterest.getKeys();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(Files.newBufferedReader(xmlFile)); //new FileInputStream(xmlFile));

        Stack<String> elementStack = new Stack<>();

        String parent = "";
        String previousParent = "";

        while (true) {
            int event = xmlStreamReader.next();
            if (event == XMLStreamConstants.END_DOCUMENT) {
                xmlStreamReader.close();
                break;
            }

            parent = !elementStack.isEmpty() ? elementStack.peek() : "";
            if (event == XMLStreamConstants.START_ELEMENT) {
                if (xmlStreamReader.getAttributeCount() > 0) {
                    for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++) {
                        String attribute = xmlStreamReader.getAttributeLocalName(i);
                        String key = parent + "." + xmlStreamReader.getLocalName();
                        String value = xmlStreamReader.getAttributeValue(i);

                        addToResults(elementAttributes, results, value, key, attribute);

                        //log.info(parent + "->" + xmlStreamReader.getLocalName() + "->" + attribute + "->" + value);
                    }
                }

                if (!parent.equals(xmlStreamReader.getLocalName())) {
                    previousParent = parent;
                } else {
                    previousParent = xmlStreamReader.getLocalName();
                }

                elementStack.push(xmlStreamReader.getLocalName());
            } else if (event == XMLStreamConstants.CHARACTERS) {
                if (!xmlStreamReader.getText().trim().isEmpty()) {
                    String key = previousParent + "." + parent;
                    String value = xmlStreamReader.getText();

                    addToResults(elementAttributes, results, value, key, "");

                    //log.info(previousParent + "->"+ parent + "->"+xmlStreamReader.getText());
                }
            } else if (event == XMLStreamConstants.END_ELEMENT) {
                elementStack.pop();
            }

        }

        /*
        for (Map.Entry<String, List<String>> val : results.entrySet()) {
            log.info(fixHeaderName(val.getKey()) + " " + val.getValue());
        }
        */
        return results;
    }

    private void addToResults(Map<String, List<String>> elementAttributes, Map<String, List<String>> results, String value, String key, String attribute) {
        Map<String, String> result = getValue(key, value, attribute, elementAttributes);
        if (!result.isEmpty()) {
            Map.Entry<String, String> entry = result.entrySet().iterator().next();

            if (!results.containsKey(entry.getKey())) {
                results.put(entry.getKey(), new ArrayList<>());
            }
            if (!results.get(entry.getKey()).contains(entry.getValue())) {
                results.get(entry.getKey()).add(entry.getValue());
            }
        }
    }

    private String fixHeaderName(String element) {
        long dotCount = element.chars().filter(ch -> ch == '.').count();
        if (dotCount > 1) {
            return element.substring(element.indexOf(".") + 1);
        }
        return element;
    }

    private Map<String, String> getValue(String key, String value, String attribute, Map<String, List<String>> elementAttributes) {
        if (elementAttributes.containsKey(key)) {
            if (elementAttributes.get(key) != null) {
                for (String attr : elementAttributes.get(key)) {
                    if (attribute.equals(attr)) {
                        return Collections.singletonMap(key + "." + attr, value);
                    }
                }
            } else {
                return Collections.singletonMap(key, value);
            }
        }

        return Collections.emptyMap();
    }
}
