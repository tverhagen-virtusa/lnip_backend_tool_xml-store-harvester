package com.lexisnexis.lnip.perftest.parser;

import java.util.List;
import java.util.Map;

public interface XmlTagsAndAttributesOfInterest {

    /**
     * The Map contains XML-Tags as keys and optional XML-Attributes as values.
     * These points of interest will be used for the data extraction.
     */
    public Map<String, List<String>> getPointsOfInterest();
    
}
