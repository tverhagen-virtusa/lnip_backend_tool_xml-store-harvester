package com.lexisnexis.lnip.perftest.parser;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class XmlTagsAndAttributesOfInterestSample {

    public Map<String, List<String>> getKeys() {
        Map<String, List<String>> results = new LinkedHashMap<>();

        results.put("univentio-patent-document.Publication", Arrays.asList("PublicationId", "Status"));
        results.put("Publication.Authority", Arrays.asList("Code"));
        results.put("Raw.Number", null);
        results.put("Raw.Kind", null);

        results.put("DomesticFamily.FamilyId", null);
        results.put("MainFamily.FamilyId", null);
        results.put("CompleteFamily.FamilyId", null);
        results.put("FullFamily.FamilyId", null);
        results.put("SimpleFamily.FamilyId", null);

        results.put("Titles.Title", Arrays.asList("Lang_ISO639-2", "DataType"));
        results.put("Claims.Claims", Arrays.asList("Lang_ISO639-2", "DataType"));
        results.put("Abstracts.Abstracts", Arrays.asList("Lang_ISO639-2", "DataType"));
        results.put("Descriptions.Description", Arrays.asList("Lang_ISO639-2", "DataType"));
        return results;
    }

}
