# Extract Data from XML documents

_Remark: This can be seen as a general tool, for data extraction and it's can be used 
on it's own. It's currently located as Maven module within file-store, as it contains 
a lot of xml documents. And the initial use for using it against this set of xml 
documents._


Sometimes it can be useful to retrieve (the same) information from multiple xml
documents. This library uses Stax to do it in a smart and fast way.