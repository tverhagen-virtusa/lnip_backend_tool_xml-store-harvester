# Possible Improvements

- [ ] Replace own xml location structure with XPath
- [ ] Create tree hierargy from points-of-interest
      - [ ] First try / load all tags from xml root tag 'univentio-patent-document'.
            This will make it easier for extracting quicker the tag text() and attribute values.
      - [ ] Use XSD for extracting all possible tags and their locations (build tree)
            Match that tree, against the tree of interest.