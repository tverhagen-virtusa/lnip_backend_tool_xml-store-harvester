# TODO

- [x] configure different XML-Store mounted directories
- [x] scan mounted directories for authorities
      create map, where each key:value : authority:path

- [x] convert publication number to Univentio path structure
      - [x] correction for short publication numbers

- [x] create output directory, into which the XML-files are copied.
      - [x] same storage as in existing XML-Store
      - [x] flat storage, pure all files in same single directory, containing all the 
            XML files

- [x] copy files, with their attributes (time of), from XML-Store to destination location

- [x] harvest from input Publication instances

- [x] harvest from input csv file:

        Id,Pid
        141866719,US10000035B2
        141866863,US10000133B2

- [ ] add support for csv format

        authority, number, kind

- [/] add command line interface (cli)
      [What library should I use for handling CLI arguments for my Java program?](https://softwarerecs.stackexchange.com/questions/16450/what-library-should-i-use-for-handling-cli-arguments-for-my-java-program)
      https://github.com/remkop/picocli/wiki/picocli-vs-JCommander

- [ ] read xml-store configuration from json / yaml file

        xml-store:
          - mount-1
          - mount-2

- [ ] create output (logging) which publications where copied successfully and which 
      ones where not.

## Nice to have

- [ ] Add test for same authority found on different locations, should throw error
      As same authority is not expected to be found in same XML-Store.

    xml-store-leiden/xml-store-1/US
    xml-store-leiden/xml-store-2/CN

    xml-store-leiden/xml-store-1/current/US
    xml-store-leiden/xml-store-1/20191120/US
    xml-store-leiden/xml-store-1/20191119/US
    xml-store-leiden/xml-store-2/current/CN
    xml-store-leiden/xml-store-2/20191120/CN
    xml-store-leiden/xml-store-2/20191119/CN

## Think about

- [ ] store the harvested XML-files in a git repo?
