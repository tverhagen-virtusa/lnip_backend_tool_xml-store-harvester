package com.lexisnexis.lnip.tools.xmlstoreharvester;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.PublicationMaker;
import com.lexisnexis.lnip.filestore.PublicationMakerRegistery;
import com.lexisnexis.lnip.filestore.UniventioFileStore;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XmlStoreHarvester {
    private static final String SEPARATOR = ",";
    private Logger logResults = LoggerFactory.getLogger("results");
    private UniventioFileStore xmlStore;
    // CSV line: id, pid
    private final static String PATTERN_CSV_ID_PID_NAME = "csv-compact";
    private final static String PATTERN_CSV_ID_PID_1ST_LINE = "^\\s*id\\s*,\\s*pid\\s*$";
    private final static String PATTERN_CSV_ID_PID_LINES = "^(?<id>[0-9]+)," + Publication.PATTERN_PUBLICATION + "$";
    private final static PublicationMaker makerCsvCompact = PublicationMaker.createPublicationMaker(
            PATTERN_CSV_ID_PID_NAME 
            , PATTERN_CSV_ID_PID_1ST_LINE
            , PATTERN_CSV_ID_PID_LINES);
    private final static String PATTERN_AUTHORITY_NUMBER_KIND_NAME = "csv-authority-number-kind";
    private final static String PATTERN_AUTHORITY_NUMBER_KIND_1ST_LINE = "^\\s*authority\\s*,\\s*number\\s*,\\s*kind\\s*$";
    private final static String PATTERN_AUTHORITY_NUMBER_KIND_LINES = "^" 
            + Publication.REX_EXP_AUTHORITY
            + SEPARATOR + Publication.REX_EXP_NUMBER
            + SEPARATOR + Publication.REX_EXP_KIND
            + "$";
    private final static PublicationMaker makerCsvAuthorityNumberKind = PublicationMaker.createPublicationMaker(
            PATTERN_AUTHORITY_NUMBER_KIND_NAME
            , PATTERN_AUTHORITY_NUMBER_KIND_1ST_LINE
            , PATTERN_AUTHORITY_NUMBER_KIND_LINES);

    private final PublicationMakerRegistery registery = new PublicationMakerRegistery();


    public XmlStoreHarvester(UniventioFileStore xmlStore) {
        this.xmlStore = xmlStore;
        registery.add(makerCsvCompact);
        registery.add(makerCsvAuthorityNumberKind);
    }


    public void harvest(Path destinationPath, Path csvSource, String publicationMakerName) throws IOException {
        harvest(destinationPath, csvSource, publicationMakerName, true);
    }
    public void harvest(Path destinationPath, Path csvSource, String publicationMakerName, boolean isWithPathStructureActive) throws IOException {
        Set<Publication> publications = createPublications(csvSource, publicationMakerName);

        harvest(destinationPath, publications, isWithPathStructureActive);
    }


    private Set<Publication> createPublications(Path csvSource, String publicationMakerName) throws IOException {
        PublicationMaker publicationMaker = registery.get(publicationMakerName); //PublicationMaker.createPublicationMaker(PATTERN_CSV_ID_PID_1ST_LINE, PATTERN_CSV_ID_PID_LINES); 
        Stream<String> lines = Files.lines(csvSource);
        Set<Publication> publications = lines
                .filter(line -> ! line.equalsIgnoreCase("id,pid"))
                .filter(line -> ! line.startsWith("#"))
                .map(line -> {
                try {
                    return publicationMaker.create(line);
                }
                catch (IllegalArgumentException iae) {
                    log.warn("Not able to create Publication from line with value '" + line + "'.");
                }
                return null;
                }).collect(Collectors.toSet());
        lines.close();
        return publications;
    }

    public void harvest(Path destinationPath, Collection<Publication> publications) throws IOException {
        harvest(destinationPath, publications, true);
    }
    public void harvest(Path destinationPath, Collection<Publication> publications, boolean isWithPathStructureActive) throws IOException {
        if (! Files.isDirectory(destinationPath)) {
            Files.createDirectories(destinationPath);
        }

        if (! Files.isWritable(destinationPath)) {
            throw new IOException("Unable to write at destination '" + destinationPath + "'.");
        }

        for (Publication publication : publications) {
            if (publication == null) {
                continue;
            }
            Path sourcePath = xmlStore.getPath(publication, "xml");
            if (! Files.exists(sourcePath)) {
                logResults.warn("No file found '" + sourcePath + "'.");
                continue;
            }
            Path xmlFileLocation = (isWithPathStructureActive) ? xmlStore.toPath(publication, "xml") : xmlStore.toFile(publication, "xml");

            try {
                copyFile(sourcePath, destinationPath.resolve(xmlFileLocation));
            }
            catch (IOException ioe) {
                log.error("Coping file '" + sourcePath + "' to '" + destinationPath.resolve(xmlFileLocation) 
                        + "' is not possible." + ioe.getMessage());
            }
        }
    }

    private void copyFile(Path sourceFile, Path destinationFile) throws IOException {
        if (isFileAlreadyCopied(sourceFile, destinationFile)) {
            return;
        }

        Path dataPath = destinationFile.getParent();
        if (! Files.exists(dataPath)) {
            Files.createDirectories(dataPath);
        }

        log.info("Copying file '" + sourceFile + "' to '" + destinationFile + "'");
        Files.copy(
                sourceFile
                , destinationFile
                , StandardCopyOption.COPY_ATTRIBUTES);
    }


    private boolean isFileAlreadyCopied(Path sourceFile, Path destinationFile) throws IOException {
        if (Files.exists(destinationFile)) {
            BasicFileAttributes destFileAttr = Files.readAttributes(destinationFile,
                    BasicFileAttributes.class);
            BasicFileAttributes sourceFileAttr = Files.readAttributes(sourceFile,
                    BasicFileAttributes.class);
            log.info("source      [ " + sourceFileAttr.size()
                    + " | " + sourceFileAttr.creationTime()
                    + " | " + sourceFileAttr.lastModifiedTime() + " ]");
            log.info("destination [ " + destFileAttr.size()
            + " | " + destFileAttr.creationTime()
            + " | " + destFileAttr.lastModifiedTime() + " ]");
            if (destFileAttr.lastModifiedTime().equals(sourceFileAttr.lastModifiedTime())
                    && destFileAttr.size() == sourceFileAttr.size()) {
                log.info("File '" + destinationFile + "' still the same.");
                return true;
            }
        }
        return false;
    }

    public static Path createDestinationPath(String pathStr) {
        String pathCln = StringUtils.trimToNull(pathStr);
        Path destPath;
        if (pathCln == null) {
            destPath = Paths.get("xml-store-harvester");
        }
        else {
            destPath = Paths.get(pathCln);
        }
        return destPath;
    }


    public void harvest(Path destinationPath, File[] sourceFiles, String publicationMakerName, boolean isKeepUniventioPathStructureActive) throws IOException {
        for (File sourceFile : sourceFiles) {
            if (sourceFile.exists() && sourceFile.canRead()) {
                harvest(destinationPath, sourceFile.toPath(), publicationMakerName, isKeepUniventioPathStructureActive); 
            }
            else {
                log.warn("Unable to read file {0}", sourceFile);
            }
        }
    }

}
