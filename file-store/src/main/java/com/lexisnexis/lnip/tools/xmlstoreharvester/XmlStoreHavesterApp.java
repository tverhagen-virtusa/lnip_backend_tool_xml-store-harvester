package com.lexisnexis.lnip.tools.xmlstoreharvester;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.lexisnexis.lnip.filestore.UniventioFileStore;
import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.ParseResult;

@Slf4j
public class XmlStoreHavesterApp {
    @Option(names = {"-n", "--no-univentio-path-structure"}, description = "If Univention path structure, is kept also as output structure.")
    private boolean isKeepUniventioPathStructureActive = true;

    @Option(names = {"-p", "--publication-maker"}, required = true, description = "Use the specific publication maker instance.")
    private String publicationMakerName;

    @Option(names = {"-d", "--destination"}, description = "Output destination")
    private String destination;

    @Parameters(paramLabel = "FILE", description = "One ore more files, which containing publication-id entries."
            + "Each publication-id results in the XML file, that will be copied.")
    private File[] files;


    public static void main(String[] args) {
        XmlStoreHavesterApp app = new XmlStoreHavesterApp();
        ParseResult parseArgs = new CommandLine(app).parseArgs(args);

        app.execute();
    }


    private void execute() {
        System.out.println("destination: '" + destination + "'");
        System.out.println("files: [ " + String.join("|", toString(files)) + " ]");
        
        String rootPathStr = "/Users/tjeerdverhagen/xml-store";
        Collection<String> fileStoreDirectories = Arrays.asList(new String[] { "xmlstore1", "xmlstore2", "xmlstore3", "xmlstore4", "xmlstore5", "xmlstore6"}) ;
        String[] fileExtensions = new String[] { "xml" };
        UniventioFileStoreConfiguration config = UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories, fileExtensions);
        
        try {
            UniventioFileStore fileStore = new UniventioFileStore(config);
            XmlStoreHarvester xmlStoreHarvester = new XmlStoreHarvester(fileStore);
            xmlStoreHarvester.harvest(Paths.get(destination), files, publicationMakerName, isKeepUniventioPathStructureActive);
        }
        catch (IOException ioe) {
            log.error("", ioe);
        }
    }


    private List<String> toString(File[] files) {
        List<String> filesStr = new ArrayList<>();
        Arrays.asList(files).stream().map(Object::toString).forEach(f -> filesStr.add(f));
        return filesStr;
    }

}
