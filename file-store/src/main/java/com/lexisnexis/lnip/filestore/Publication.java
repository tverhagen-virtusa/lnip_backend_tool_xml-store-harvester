package com.lexisnexis.lnip.filestore;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Publication {
    public final static String REX_EXP_AUTHORITY = "(?<authority>[A-Z]{2})";
    public final static String REX_EXP_NUMBER = "(?<number>[A-Z0-9]+?(?:[A-Z]{3})?)";
    public final static String REX_EXP_KIND = "(?<kind>[A-Z][A-Z0-9]?)";
    public final static Pattern PATTERN_PUBLICATION = Pattern.compile(REX_EXP_AUTHORITY + REX_EXP_NUMBER + REX_EXP_KIND,
            Pattern.CASE_INSENSITIVE);
    private final String authority;
    private final String number;
    private final String kind;

    
    public static Publication create(String publicationStr) {
        Matcher matcher = PATTERN_PUBLICATION.matcher(publicationStr);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Argument 'publicationStr' with value '" + publicationStr + "' is not a match, for the regular expression '" 
                    + PATTERN_PUBLICATION + "'");
        }
        return new Publication(matcher.group("authority"), matcher.group("number"), matcher.group("kind"));
    }
    public Publication(String authority, String number, String kind) {
        
        this.authority = authority;
        this.number = number;
        this.kind = kind;
    }

    public String getAuthority() {
        return authority;
    }

    public String getNumber() {
        return number;
    }

    public String getKind() {
        return kind;
    }
    
    public String getPid() {
        return authority + number + kind;
    }

    @Override
    public String toString() {
        return authority + "-" + number + "-" + kind;
    }
}
