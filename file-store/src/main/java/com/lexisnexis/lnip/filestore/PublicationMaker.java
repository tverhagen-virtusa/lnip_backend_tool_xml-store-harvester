package com.lexisnexis.lnip.filestore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PublicationMaker {
    private final String name;
    private final Pattern pattern1stLine;
    private final Pattern patternLines;


    public PublicationMaker(String name, Pattern pattern1stLine, Pattern patternLines) {
        this.name = name;
        this.pattern1stLine = pattern1stLine;
        this.patternLines = patternLines;
    }

    public static PublicationMaker createPublicationMaker(String name, String regExp1stLine, String regExpLines) {
        return new PublicationMaker(
                name
                , Pattern.compile(regExp1stLine, Pattern.CASE_INSENSITIVE)  // RegexOptions.Compiled | RegexOptions.Singleline);
                , Pattern.compile(regExpLines, Pattern.CASE_INSENSITIVE)  // RegexOptions.Compiled | RegexOptions.Singleline);
                );
    }


    public String getName() {
        return name;
    }

    public boolean isMatching(String firstLine) {
        return pattern1stLine.matcher(firstLine).matches();
    }

    public Publication create(String text) {
        Matcher matcher = patternLines.matcher(text);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Argument 'text' with value '" + text + "' is not a match, for the regular expression '" 
                    + patternLines + "'");
        }
        return new Publication(matcher.group("authority"), matcher.group("number"), matcher.group("kind"));
    }

}
