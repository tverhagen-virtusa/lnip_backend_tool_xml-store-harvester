package com.lexisnexis.lnip.filestore.command;

import picocli.CommandLine;

public class ParameterException extends Exception {
    private CommandLine commandLine;


    public ParameterException(CommandLine commandLine, String text) {
        super(text);
        this.commandLine = commandLine;
    }

}
