package com.lexisnexis.lnip.filestore;

public enum MediaType {
    APPLICATION_XML("application/xml")
    , APPLICATION_JSON("application/json");

    private final String type;
    private final String subType;
    private final String fullType;

    private MediaType(String text) {
        this.type = text.substring(0, text.indexOf('/'));
        this.subType = text.substring(text.indexOf('/') + 1);
        this.fullType = type + "/" + subType;
    }

    public String getType() {
        return type;
    }

    public String getSubType() {
        return subType;
    }

    public String getFullType() {
        return fullType;
    }
}
