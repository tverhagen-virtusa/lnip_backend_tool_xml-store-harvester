package com.lexisnexis.lnip.filestore;

import org.apache.commons.lang3.StringUtils;

public class Authority {
    private final String id;

    public Authority(String id) {
        String idCln = StringUtils.trimToNull(id);
        if (idCln == null) {
            throw new IllegalArgumentException("Argument 'id' should not be null.");
        }
        this.id = idCln;
    }

    public String getId() {
        return id;
    }

}
