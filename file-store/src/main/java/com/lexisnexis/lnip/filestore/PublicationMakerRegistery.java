package com.lexisnexis.lnip.filestore;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class PublicationMakerRegistery {
    private Map<String, PublicationMaker> publicationMakers = new HashMap<>();


    public void add(PublicationMaker publicationMaker) {
        if (publicationMakers.containsKey(publicationMaker.getName())) {
            throw new IllegalArgumentException("Argument 'PublicationMaker' has name with value '" + publicationMaker.getName() + "', which has already been registered.");
        }
        publicationMakers.put(publicationMaker.getName(), publicationMaker);
    }

    public Set<String> getNames() {
        return Collections.unmodifiableSet(publicationMakers.keySet());
    }

    public PublicationMaker get(String name) {
        String nameCln = StringUtils.trimToNull(name);
        if (nameCln == null) {
            throw new IllegalArgumentException("Argument 'name' should no be null.");
        }

        if (! publicationMakers.containsKey(nameCln)) {
            throw new IllegalArgumentException("Argument 'name' with value '" + nameCln + "' is not a known PublicationMaker. Known names: [ " 
                    + String.join(", ", publicationMakers.keySet()) + " ]");
        }
        return publicationMakers.get(name);
    }

}
