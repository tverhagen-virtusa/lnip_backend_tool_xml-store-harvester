package com.lexisnexis.lnip.filestore;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UniventioFileStore {
    private final static int AUTHORITY_PATH_LENGTH = 2;
    private UniventioFileStoreConfiguration cfg;
    private Map<String, Path> authorityPaths = new HashMap<>();
    private UniventioPathStructure univentioPathStructure = new UniventioPathStructure();

    public UniventioFileStore(UniventioFileStoreConfiguration cfg) throws IOException {
        if (cfg == null) {
            throw new IllegalArgumentException("Argument 'null' should not be null.");
        }
        this.cfg = cfg;

        lookupXmlStorePathsForAuthoriyEntries();
    }


    /**
     * Each xmlStorePath should contain a unique set of Authority entries 
     * @throws IOException 
     */
    private void lookupXmlStorePathsForAuthoriyEntries() {
        for (Path path : cfg.getFileStorePaths()) {
            if (! Files.exists(path)) {
                continue;
            }
            for (File entry : path.toFile().listFiles()) {
                if (entry.isDirectory()) {
                    if (entry.getName().length() == AUTHORITY_PATH_LENGTH) {
                        authorityPaths.put(entry.getName().toUpperCase(), path);
                    }
                    else {
                        log.info("Scanning for authority directories at location '" + path 
                                + "', found directory '" + entry.getName() 
                                + "', but not adding it as authority as they are restricked to a length of " 
                                + AUTHORITY_PATH_LENGTH + ".");
                    }
                }
            }
        }
    }

    public Set<String> getAuthorities() {
        return Collections.unmodifiableSet(authorityPaths.keySet());
    }

    public Path getPath(String authority) {
        String authorityCln = StringUtils.trimToNull(authority);

        if (! authorityPaths.containsKey(authorityCln)) {
            throw new IllegalArgumentException("Argument 'authority' with value '" + authorityCln + "' is not a known authority.");
        }
        return authorityPaths.get(authorityCln);
    }
    
    public Path getPath(Publication publication, String extension) {
        return getPath(publication.getAuthority()).resolve(toPath(publication, extension));
    }
    public void createPath(Publication publication) throws IOException {
        Path path = toPath(publication, null);
        if (! authorityPaths.containsKey(publication.getAuthority().toUpperCase())) {
            log.info("Needing to create Univentio structure directory '" + path.getParent() + "'.");
            Files.createDirectories(createAuthorityPath(publication.getAuthority()).resolve(path).getParent());
        }
        Path baseDestPath = authorityPaths.get(publication.getAuthority().toUpperCase());
        Path destPath = baseDestPath.resolve(path.getParent());
        if (! Files.exists(destPath)) {
            Files.createDirectories(destPath);
        }
    }

    private Path createAuthorityPath(String authority) throws IOException {
        if (! authorityPaths.containsKey(authority.toUpperCase())) {
            // TODO Add preferred data locations per Authority + Univentio path structure
            Path dataPath = cfg.getFileStorePaths().iterator().next().toAbsolutePath();
            Files.createDirectories(dataPath);
            authorityPaths.put(authority.toUpperCase(), dataPath.getParent());
            log.info("Creating for authority '" + authority.toUpperCase() + "' data path '"
                    + authorityPaths.get(authority.toUpperCase()) + "'.");
        }
        return authorityPaths.get(authority.toUpperCase());
    }


    public Path toPath(Publication publication, String extension) {
        if (publication == null) {
            throw new IllegalArgumentException("Argument 'publication' should not be null.");
        }
        if (extension == null) {
            extension = cfg.getDefaultFileExtention();
        }

        return univentioPathStructure.toPath(publication, extension);
    }

    public Path toFile(Publication publication, String extension) {
        return Paths.get(publication.getPid() + "." + extension);
    }

}
