package com.lexisnexis.lnip.filestore;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class UniventioPathStructure {
    private static final String DEFAULT_PATH_PART = "00";
    private final Pattern patternDigits = Pattern.compile("^\\d{2}$"); 


    public Path toPath(Publication publication, String extention) {
        return Paths.get(publication.getAuthority(), 
                toPathStructure(publication).toString(), 
                toFile(publication, extention).toString());
    }

    public Path toFile(Publication publication, String extention) {
        return Paths.get(publication.getPid() + "." + extention);
    }

    public Path toPathStructure(Publication publication) {
        return toPathStructure(publication.getNumber());
    }

    Path toPathStructure(String number) {
        return Paths.get(
                getPart(number, number.length() - 7, 2), 
                getPart(number, number.length() - 5, 2), 
                (Integer.parseInt(substring(number, number.length() - 3, 3)) < 500) ? "D1" : "D2");
    }

    /**
     * Returns a string that is a substring of text. The substring begins at the start position
     * and ends at the start + length position.
     * If start < 0, then {@linkplain UniventioPathStructure#DEFAULT_PATH_PART} is returned.
     * If substring are not 2 digits then {@linkplain UniventioPathStructure#DEFAULT_PATH_PART} is returned.
     * @param text
     * @param start - start position
     * @param offset - the number of characters to get
     * @return
     */
    private String getPart(String text, int start, int offset) {
        if (start < 0) {
            return DEFAULT_PATH_PART;
        }
        String part = substring(text, start, offset);
        
        return patternDigits.matcher(part).matches() ? part : DEFAULT_PATH_PART;
    }

    /**
     * Returns a string that is a substring of text. The substring begins at the start position
     * and has the length of the offset.
     * @param text
     * @param start - start position
     * @param offset - the number of characters to get
     * @return
     */
    private String substring(String text, int start, int offset) {
        return text.substring(start, start + offset);
    }

}
