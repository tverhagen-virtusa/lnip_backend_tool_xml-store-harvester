package com.lexisnexis.lnip.filestore.command;

import java.io.IOException;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine.Command;

@Command(name = "copy", description = "Copy files from File Store to destination.")
public class StoreManagerCopy implements Callable<Integer> {
    private static Logger logger = LoggerFactory.getLogger(StoreManagerCopy.class);


    @Override
    public Integer call() throws Exception {
        logger.info("Inside copy...");
        
        logger.error("Copy is not yet implemented...");
//        throw new RuntimeException("Not yet implemented...");
        return 1;
    }

}
