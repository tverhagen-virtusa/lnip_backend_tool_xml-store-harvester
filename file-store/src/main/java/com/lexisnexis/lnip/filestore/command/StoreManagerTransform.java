package com.lexisnexis.lnip.filestore.command;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;

import org.apache.commons.io.IOUtils;

import com.lexisnexis.lnip.filestore.MediaType;
import com.lexisnexis.lnip.importer.config.TransformConfig;
import com.lexisnexis.lnip.importer.config.TransformConfig.TransformationName;
import com.lexisnexis.lnip.importer.services.TransformationService;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "transform", description = "Use XLST Transformation")
public class StoreManagerTransform implements Callable<Integer> {
    private final Map<MediaType, String> mediaTypeToExtension = new HashMap<>();

    @Option(names = {"-k", "--keep-univentio-path-structure"}, description = "If Univention path structure, is kept also as output structure.")
    private boolean isKeepUniventioPathStructureActive = true;

//    @Option(names = {"-p", "--publication-maker"}, required = true, description = "Use the specific publication maker instance.")
//    private String publicationMakerName;

    @Option(names = {"-d", "--destination"}, description = "Output destination")
    private String destination;

    @Option(names = {"-t", "--transformation-name"}, description = "The XSLT to use.")
    private String transformationName;

    @Parameters(paramLabel = "FILE", description = "One ore more files, which containing publication-id entries."
            + "Each publication-id results in the XML file, that will be copied.")
    private File[] files;


    private TransformConfig transformConfig = new TransformConfig();
    private TransformationService transformationService;


    public StoreManagerTransform() throws TransformerConfigurationException, JAXBException {
        transformationService = new TransformationService(transformConfig.jaxbContext(), transformConfig.getTemplates());

        mediaTypeToExtension.put(MediaType.APPLICATION_XML, "xml");
        mediaTypeToExtension.put(MediaType.APPLICATION_JSON, "json");
    }



    @Override
    public Integer call() throws Exception {
        for (File file : files) {
            String filename = file.getName();
            Path target = Paths.get(destination);
            Files.createDirectories(target);
            Path newFilename = target.resolve(replaceExtension(filename, MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML));

            InputStream is = new FileInputStream(file);
            ByteArrayOutputStream os = transformationService.transform(TransformationName.SEARCH, IOUtils.toByteArray(is));
            try (OutputStream outputStream = new FileOutputStream(newFilename.toFile())) {
                os.writeTo(outputStream);
            }
        }
        return 0;
    }


    public String replaceExtension(String filename, MediaType extToAdd, MediaType... extToRemove) {
        return addExtension(removeExtension(filename, extToRemove), extToAdd);
    }

    public String removeExtension(String filename, MediaType... extToRemove) {
        if (filename.indexOf(".") < 0) {
            return filename;
        }

        Set<String> extensions = new HashSet<>();
        for (MediaType mediaType : extToRemove) {
            extensions.add(mediaTypeToExtension.get(mediaType));
        }
        return removeExtension(filename, extensions.toArray(new String[0]));
    }
    public String removeExtension(String filename, String... extToRemove) {
        if (filename.indexOf(".") < 0) {
            return filename;
        }

        List<String> extensions = Arrays.asList(extToRemove);
        String ext = filename.substring(filename.lastIndexOf('.') + 1);
        if (extensions.contains(ext)) {
            return filename.substring(0, filename.lastIndexOf('.'));
        }
        return filename;
    }

    public String addExtension(String filename, MediaType mediaType) {
        return filename + "." + mediaTypeToExtension.get(mediaType);
    }

}
