package com.lexisnexis.lnip.filestore.command;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lexisnexis.lnip.tools.xmlstoreharvester.XmlStoreHavesterApp;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.ParseResult;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Spec;

@Command(
    name = "ufs"
    , subcommands = {
            StoreManagerCopy.class
            , StoreManagerTransform.class
    }
)
public class UniventioFileStoreApp implements Callable<Integer> {
    @Spec
    private CommandSpec spec;
    private static Logger logger = LoggerFactory.getLogger(UniventioFileStoreApp.class);

    public static void main(String[] args) {
        try {
            mainExecution(args);
        }
        catch (Exception e) {
            logger.error("Error ...", e);
            System.exit(1);
        }
    }

    public static Integer mainExecution(String... args) throws Exception {
//        UniventioFileStoreApp app = new UniventioFileStoreApp();
//        CommandLine cmdLine = new CommandLine(app);
////        ParseResult parseArgs = cmdLine.parseArgs(args);
//
//        return cmdLine.execute(args);

        return new CommandLine(new UniventioFileStoreApp()).execute(args);
    }

    @Override
    public Integer call() throws Exception {
        throw new ParameterException(spec.commandLine(), "Missing required subcommand");
    }

}
