package com.lexisnexis.lnip.filestore.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UniventioFileStoreConfiguration {
    private final static String ENVIRONMENT_PRODUCTION = "production";

    private final String name;
    private final String environment;
    private final Set<Path> fileStorePaths;
    private final String defaultFileExtention;
    private final Set<String> fileExtentions = new HashSet<>();


    public UniventioFileStoreConfiguration(String name, String environment, Collection<Path> fileStorePaths, Collection<String> fileExtentions) {
        String nameCln = StringUtils.trimToNull(name);
        if (nameCln == null) {
            throw new IllegalArgumentException("Argument 'name' should not be null.");
        }
        this.name = nameCln;
        String environmentCln = StringUtils.trimToNull(environment);
        if (environmentCln == null) {
            environmentCln = ENVIRONMENT_PRODUCTION;
        }
        this.environment = environmentCln;
        if (fileStorePaths == null) {
            throw new IllegalArgumentException("Argument 'fileStorePaths' should not be null.");
        }
        if (fileStorePaths.isEmpty()) {
            throw new IllegalArgumentException("Argument 'fileStorePaths' should not be empty.");
        }
        this.fileStorePaths = new HashSet<>();
        this.fileStorePaths.addAll(fileStorePaths);

        if (fileExtentions == null) {
            throw new IllegalArgumentException("Argument 'fileExtentions' should not be null.");
        }
        if (fileExtentions.isEmpty()) {
            throw new IllegalArgumentException("Argument 'fileExtentions' should not be empty.");
        }
        String defaultFileExtentionTmp = null;
        for (String fileExt : fileExtentions) {
            String fileExtCln = StringUtils.trimToNull(fileExt);
            if (fileExtCln == null) {
                continue;
            }
            if (defaultFileExtentionTmp == null) {
                defaultFileExtentionTmp = fileExtCln;
            }
            this.fileExtentions.add(fileExtCln);
        }
        defaultFileExtention = defaultFileExtentionTmp;

        // If defaultFileExtention still null, there was no content in argument fileExtentions
        if (defaultFileExtention == null) {
            throw new IllegalArgumentException("Argument 'fileExtentions' should not be empty.");
        }
    }

    public static UniventioFileStoreConfiguration create(String rootPathStr, Collection<String> fileStoreDirectories, String... fileExtensions) {
        return create(null, null, rootPathStr, fileStoreDirectories, Arrays.asList(fileExtensions));
    }

    public static UniventioFileStoreConfiguration create(String name, String environment, String rootPathStr, Collection<String> fileStoreDirectories, String... fileExtensions) {
        return create(name, environment, rootPathStr, fileStoreDirectories, Arrays.asList(fileExtensions));
    }
    public static UniventioFileStoreConfiguration create(String name, String environment, String rootPathStr, Collection<String> fileStoreDirectories, Collection<String> fileExtensions) {
        final String rootPathCln = StringUtils.trimToNull(rootPathStr);
        if (rootPathCln == null) {
            throw new IllegalArgumentException("Argument 'rootPathStr' should not be null.");
        }
        String nameCln = (StringUtils.trimToNull(name) != null) ? StringUtils.trimToNull(name) : rootPathCln;
        String environmentCln = (StringUtils.trimToNull(environment) != null) ? StringUtils.trimToNull(environment) : ENVIRONMENT_PRODUCTION;

        Path rootPath = Paths.get(rootPathCln);
        if (! rootPath.toFile().exists() || ! rootPath.toFile().isDirectory()) {
            if (ENVIRONMENT_PRODUCTION.equalsIgnoreCase(environmentCln)) {
                throw new IllegalArgumentException("Argument 'rootPathStr' with value '" + rootPath + "' does not exist on the file system or is not a directory"
                        + ", at '" + new File(System.getProperty("user.dir")).getAbsolutePath() + "'.");
            }
            else {
                log.info("On environment ''{0}'' creating file store path ''{1}''.", environmentCln, rootPath);
                try {
                    Files.createDirectories(rootPath);
                }
                catch (IOException e) {
                    throw new RuntimeException(MessageFormat.format("Unable to create directory ''{0}''.", rootPath), e);
                }
            }
        }
        
        if (fileStoreDirectories == null) {
            throw new IllegalArgumentException("Argument 'fileStoreDirectories' should not be null.");
        }
        if (fileStoreDirectories.isEmpty()) {
            throw new IllegalArgumentException("Argument 'fileStoreDirectories' should not be empty.");
        }
        
        List<Path> fileStorePaths = new ArrayList<>(); 
        for (String dirStr : fileStoreDirectories) {
            String dirCln = StringUtils.trimToNull(dirStr);
            if (dirCln == null) {
                continue;
            }
            Path xmlStorePath = rootPath.resolve(dirCln);
            if (! Files.exists(xmlStorePath)) {
                log.warn("Given File Store path '" + dirCln + "' can not be resolved.");
            }
            fileStorePaths.add(xmlStorePath);
        }
        return new UniventioFileStoreConfiguration(nameCln, environmentCln, fileStorePaths, fileExtensions);
    }

    public String getName() {
        return name;
    }

    public String getEnvironment() {
        return environment;
    }

    public Set<Path> getFileStorePaths() {
        return Collections.unmodifiableSet(fileStorePaths);
    }

    public String getDefaultFileExtention() {
        return defaultFileExtention;
    }

    public Set<String> getFileExtentions() {
        return Collections.unmodifiableSet(fileExtentions);
    }

}
