package com.lexisnexis.lnip.tools.xmlstoreharvester;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.UniventioFileStore;
import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class XmlStoreHarvesterTest {
    @Before
    public void setUp() {
        removeDirectoryRecursively(Paths.get("target", "xml-store-target"));
    }

    @Test
    public void createDestinationPathNull() throws Exception {
        assertEquals("xml-store-harvester", XmlStoreHarvester.createDestinationPath(null).toString());
    }

    @Test
    public void createDestinationPathTarget() throws Exception {
        assertEquals("target/output", XmlStoreHarvester.createDestinationPath("target/output").toString());
    }


    @Test
    public void harvest() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store";
        List<String> fileStoreDirectories = Arrays.asList(new String[] {"s1", "s5", "s6"});
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories, "xml");
        UniventioFileStore fileStore = new UniventioFileStore(fileStoreCfg);

        XmlStoreHarvester harvester = new XmlStoreHarvester(fileStore);
        List<Publication> publications = Arrays.asList(
                Publication.create("US10000253B1")
                , Publication.create("US10000085B2")
                , Publication.create("US20170000136A1")
                );
        harvester.harvest(Paths.get("target", "xml-store-target"), publications);
        // Redo same, should give no issues
        harvester.harvest(Paths.get("target", "xml-store-target"), publications);
    }

    @Test
    public void harvestInputFile() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store";
        List<String> xmlStoreDirectories = Arrays.asList(new String[] {"s1", "s5", "s6"});
        UniventioFileStoreConfiguration xmlStoreCfg = UniventioFileStoreConfiguration.create(rootPathStr, xmlStoreDirectories, "xml");
        UniventioFileStore fileStore = new UniventioFileStore(xmlStoreCfg);

        XmlStoreHarvester harvester = new XmlStoreHarvester(fileStore);
        Path inputFile = Paths.get("src/test/resources/data", "xml-store-sample.csv");
        harvester.harvest(Paths.get("target", "xml-store-target"), inputFile, "csv-compact");
    }

    @Test
    public void harvestInputFileFlatStructure() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store";
        List<String> fileStoreDirectories = Arrays.asList(new String[] {"s1", "s5", "s6"});
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories, "xml");
        UniventioFileStore fileStore = new UniventioFileStore(fileStoreCfg);

        XmlStoreHarvester harvester = new XmlStoreHarvester(fileStore);
        Path inputFile = Paths.get("src/test/resources/data", "xml-store-sample.csv");
        harvester.harvest(Paths.get("target", "xml-store-target"), inputFile, "csv-compact", false);
    }

    
    private void removeDirectoryRecursively(Path rootPath) {
        try (Stream<Path> walk = Files.walk(rootPath)) {
            walk.sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .peek(System.out::println)
                .forEach(File::delete);
        }
        catch (IOException ioe) {
            log.error("Unable to remove directory, with content.", ioe);            
        }
    }
}
