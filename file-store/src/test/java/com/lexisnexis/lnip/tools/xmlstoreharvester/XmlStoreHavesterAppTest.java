package com.lexisnexis.lnip.tools.xmlstoreharvester;

import static com.lexisnexis.lnip.filestore.command.AppUtil.add;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class XmlStoreHavesterAppTest {

    @Test
    @Ignore  // Depends on mounts on the LexisNexis network
    public void harvestXmlFromShares() {
        List<String> commandLine = new ArrayList<>();
//        add(commandLine, "--no-univentio-path-structure");
        add(commandLine, "--publication-maker", "csv-authority-number-kind");
//        add(commandLine, "--configuration ", "");
        add(commandLine, "--destination", "target/xml-store-harvester/output-sample");
//        add(commandLine, "src/test/resources/data/publication-id-comma.csv");
        add(commandLine, "src/test/resources/data/tpo-pubs-elasticsearch.csv");
        XmlStoreHavesterApp.main(commandLine.toArray(new String[0]));
    }


    @Test
    @Ignore
    public void convertXmlToJson() {
        List<String> commandLine = new ArrayList<>();
        add(commandLine, "--publication-maker", "csv-authority-number-kind");
//        add(commandLine, "--configuration ", "");
        add(commandLine, "--destination", "target/json-store/output-sample");
        add(commandLine, "src/test/resources/data/publication-id-comma.csv");
        XmlStoreHavesterApp.main(commandLine.toArray(new String[0]));
    }

}
