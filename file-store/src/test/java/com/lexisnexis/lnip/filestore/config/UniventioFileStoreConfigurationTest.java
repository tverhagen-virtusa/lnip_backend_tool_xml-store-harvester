package com.lexisnexis.lnip.filestore.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

public class UniventioFileStoreConfigurationTest {

    @Test
    public void createNoRoot() {
        String rootPathStr = " ";
        List<String> fileStoreDirectories = null;
        try {
            UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'rootPathStr' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void createNotExistingDir() {
        String rootPathStr = "does-not-exist";
        List<String> fileStoreDirectories = null;
        try {
            UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories);
            fail();
        }
        catch (IllegalArgumentException iae) {
            String expectedMsg = MessageFormat.format(
                    "Argument ''rootPathStr'' with value ''does-not-exist'' does not exist on the file system or is not a directory, at ''{0}''."
                    , System.getProperty("user.dir")); 
            assertEquals(expectedMsg, iae.getMessage());
        }
    }

    @Test
    public void createFileInsteadOfDir() {
        String rootPathStr = "pom.xml";
        List<String> fileStoreDirectories = null;
        try {
            UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories);
            fail();
        }
        catch (IllegalArgumentException iae) {
            String expectedMsg = MessageFormat.format(
                    "Argument ''rootPathStr'' with value ''pom.xml'' does not exist on the file system or is not a directory, at ''{0}''."
                    , System.getProperty("user.dir")); 
            assertEquals(expectedMsg, iae.getMessage());
        }
    }

    @Test
    public void createNoDirectories() {
        String rootPathStr = "src/test/resources/data/xml-store-big";
        List<String> fileStoreDirectories = null;
        try {
            UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileStoreDirectories' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void createDirectoriesEmpty() {
        String rootPathStr = "src/test/resources/data/xml-store-big";
        List<String> fileStoreDirectories = Collections.emptyList();
        try {
            UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileStoreDirectories' should not be empty.", iae.getMessage());
        }
    }

    @Test
    public void create() {
        String rootPathStr = "src/test/resources/data/xml-store-big";
        List<String> fileStoreDirectories = Arrays.asList(new String[] {"xml-store-1", "xml-store-2", "xml-store-2", "does-not-exist", ""});
        UniventioFileStoreConfiguration fileStoreCfg = UniventioFileStoreConfiguration.create(rootPathStr, fileStoreDirectories, "xml", " ", "xml", " json ");
        Set<Path> fileStorePaths = fileStoreCfg.getFileStorePaths();

        assertEquals(3, fileStorePaths.size());
        Set<String> fileExtentions = fileStoreCfg.getFileExtentions();
        assertEquals(2, fileExtentions.size());
        assertTrue(fileExtentions.contains("xml"));
        assertTrue(fileExtentions.contains("json"));

        assertEquals("xml", fileStoreCfg.getDefaultFileExtention());
    }

    @Test
    public void createFileStoreConfigurationNull() {
        try {
            new UniventioFileStoreConfiguration("sample-name", null, null, null);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileStorePaths' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void createFileStoreConfigurationNameEmpty() {
        try {
            new UniventioFileStoreConfiguration(null, null, Collections.emptyList(), null);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'name' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void createFileStoreConfigurationEmpty() {
        try {
            new UniventioFileStoreConfiguration("sample-name", null, Collections.emptyList(), null);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileStorePaths' should not be empty.", iae.getMessage());
        }
    }
    
    @Test
    public void createFileStoreConfigurationPathsNull() {
        List<Path> fileStorePaths = Arrays.asList(new Path[] { Paths.get("xml-store-1") });
        try {
            new UniventioFileStoreConfiguration("sample-name", null, fileStorePaths, null);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileExtentions' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void createFileStoreConfigurationPathsEmpty() {
        List<Path> fileStorePaths = Arrays.asList(new Path[] { Paths.get("xml-store-1") });
        List<String> fileExtesions = Collections.emptyList();
        try {
            new UniventioFileStoreConfiguration("sample-name", null, fileStorePaths, fileExtesions);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileExtentions' should not be empty.", iae.getMessage());
        }
    }

    @Test
    public void createFileStoreConfigurationPathsExtensionsEmpty() {
        List<Path> fileStorePaths = Arrays.asList(new Path[] { Paths.get("xml-store-1") });
        List<String> fileExtesions = Arrays.asList(new String[] { " " });
        try {
            new UniventioFileStoreConfiguration("sample-name", null, fileStorePaths, fileExtesions);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'fileExtentions' should not be empty.", iae.getMessage());
        }
    }
}
