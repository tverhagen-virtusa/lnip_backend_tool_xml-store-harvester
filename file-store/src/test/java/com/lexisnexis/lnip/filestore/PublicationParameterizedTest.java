package com.lexisnexis.lnip.filestore;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.lexisnexis.lnip.filestore.Publication;

@RunWith(Parameterized.class)
public class PublicationParameterizedTest {
    private String authority;
    private String number;
    private String kind;
    private String publicationIdStr;


    public PublicationParameterizedTest(String authority, String number, String kind, String publicationIdStr) {
        this.authority = authority;
        this.number = number;
        this.kind = kind;
        this.publicationIdStr = publicationIdStr;
    }


    @Test
    public void create() {
        Publication publication = Publication.create(publicationIdStr); 

        assertEquals(authority, publication.getAuthority());
        assertEquals(number, publication.getNumber());
        assertEquals(kind, publication.getKind());
    }


    @Parameters(name = "Run {index}: authority={0}, number={1}, kind={2}, publication-id={3}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] { 
                { "US", "20170000136", "A1", "US20170000136A1" }
                , { "US", "X9483", "A", "USX9483A" }
                , { "CN", "106008355", "B", "CN106008355B" }
                , { "JP", "6509393", "B2", "JP6509393B2" }
                , { "JO", "1005", "B", "JO1005B" }
                , { "AU", "PS104302", "D0", "AUPS104302D0" }
                });
    }

}
