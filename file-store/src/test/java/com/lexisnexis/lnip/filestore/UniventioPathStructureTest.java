package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.UniventioPathStructure;

@RunWith(Parameterized.class)
public class UniventioPathStructureTest {
    private String publicationIdStr;
    private String pathStructure;

    public UniventioPathStructureTest(String publicationId, String pathStructure) {
        this.publicationIdStr = publicationId;
        this.pathStructure = pathStructure;
    }

    @Test
    public void toPath() {
        Publication publication = Publication.create(publicationIdStr);
        assertEquals(pathStructure, new UniventioPathStructure().toPathStructure(publication).toString());
    }


    @Parameters(name = "Run {index}: publicationId={0}, pathStructure={1}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(new Object[][] {
            {"US12345678B2", "23/45/D2"}
            , {"JO1501B", "00/00/D2"}
            , {"JO1019B", "00/00/D1"}
            , {"AUPS104302D0", "00/04/D1"}
            , {"AUPS305802D0", "00/05/D2"}
        });
    }
}
