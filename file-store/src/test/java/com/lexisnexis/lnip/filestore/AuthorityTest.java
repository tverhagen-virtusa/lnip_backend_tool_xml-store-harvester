package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.*;

import org.junit.Test;

import com.lexisnexis.lnip.filestore.Authority;

public class AuthorityTest {

    @Test(expected = IllegalArgumentException.class)
    public void createNull() throws Exception {
        new Authority(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createEmpty() throws Exception {
        new Authority(" ");
    }

    @Test
    public void createUS() throws Exception {
        Authority authority = new Authority("US");
        assertEquals("US", authority.getId());
    }
}
