package com.lexisnexis.lnip.filestore.command;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBException;
import javax.xml.transform.TransformerConfigurationException;

import org.junit.Test;

import com.lexisnexis.lnip.filestore.MediaType;

public class StoreManagerTransformTest {
    private StoreManagerTransform command;


    public StoreManagerTransformTest() throws TransformerConfigurationException, JAXBException {
        command = new StoreManagerTransform();
    }


    @Test
    public void add() {
        assertEquals("some.other.json", command.addExtension("some.other", MediaType.APPLICATION_JSON));
    }

    @Test
    public void remove() {
        assertEquals("some.other", command.removeExtension("some.other", MediaType.APPLICATION_JSON));
    }

    @Test
    public void replaceExtension() {
        assertEquals("some.other.json", command.replaceExtension("some.other.xml", MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML));

        assertEquals("document.pdf.xml", command.replaceExtension("document.pdf.json", MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON));
    }
}
