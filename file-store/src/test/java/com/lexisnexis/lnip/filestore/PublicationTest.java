package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PublicationTest {

    @Test
    public void createIncorrect() {
        try { 
            Publication.create("12345678B2");
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'publicationStr' with value '12345678B2' is not a match, for the regular expression "
                    + "'(?<authority>[A-Z]{2})(?<number>[A-Z0-9]+?(?:[A-Z]{3})?)(?<kind>[A-Z][A-Z0-9]?)'", iae.getMessage());
        }
    }

}
