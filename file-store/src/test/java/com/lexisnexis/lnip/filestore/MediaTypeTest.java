package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.*;

import org.junit.Test;

public class MediaTypeTest {

    @Test
    public void getTypes() {
        assertEquals("application", MediaType.APPLICATION_JSON.getType());
        assertEquals("json", MediaType.APPLICATION_JSON.getSubType());
        assertEquals("application/json", MediaType.APPLICATION_JSON.getFullType());
    }
}
