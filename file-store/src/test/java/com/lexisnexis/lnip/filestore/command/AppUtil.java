package com.lexisnexis.lnip.filestore.command;

import java.util.List;

public class AppUtil {

    public static List<String> add(List<String> commandLine, String optionName, String optionValue) {
        commandLine.add(optionName);
        commandLine.add(optionValue);
        return commandLine;
    }
    public static List<String> add(List<String> commandLine, String parameter) {
        commandLine.add(parameter);
        return commandLine;
    }

}
