package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class PublicationMakerRegisteryTest {
    private static final String SEPARATOR = ",";

    private final static String PATTERN_CSV_ID_PID_NAME = "csv-compact";
    private final static String PATTERN_CSV_ID_PID_1ST_LINE = "^\\s*id\\s*,\\s*pid\\s*$";
    private final static String PATTERN_CSV_ID_PID_LINES = "^(?<id>[0-9]+)," + Publication.PATTERN_PUBLICATION + "$";
    private final static PublicationMaker makerCsvCompact = PublicationMaker.createPublicationMaker(
            PATTERN_CSV_ID_PID_NAME 
            , PATTERN_CSV_ID_PID_1ST_LINE
            , PATTERN_CSV_ID_PID_LINES);

    private final static String PATTERN_AUTHORITY_NUMBER_KIND_NAME = "csv-authority-number-kind";
    private final static String PATTERN_AUTHORITY_NUMBER_KIND_1ST_LINE = "^\\s*authority\\s*,\\s*number\\s*,\\s*kind\\s*$";
    private final static String PATTERN_AUTHORITY_NUMBER_KIND_LINES = "^" 
            + Publication.REX_EXP_AUTHORITY
            + SEPARATOR + Publication.REX_EXP_NUMBER
            + SEPARATOR + Publication.REX_EXP_KIND
            + "$";
    private final static PublicationMaker makerCsvAuthorityNumberKind = PublicationMaker.createPublicationMaker(
            PATTERN_AUTHORITY_NUMBER_KIND_NAME
            , PATTERN_AUTHORITY_NUMBER_KIND_1ST_LINE
            , PATTERN_AUTHORITY_NUMBER_KIND_LINES);

    private PublicationMakerRegistery registery;


    @Before
    public void setUp() {
        registery = new PublicationMakerRegistery();
        registery.add(makerCsvCompact);
        registery.add(makerCsvAuthorityNumberKind);
    }


    @Test
    public void names() {
        PublicationMakerRegistery registery = new PublicationMakerRegistery();
        registery.add(makerCsvCompact);
        registery.add(makerCsvAuthorityNumberKind);

        Set<String> names = registery.getNames();
        assertEquals(2, names.size());
        assertTrue(names.contains("csv-compact"));
        assertTrue(names.contains("csv-authority-number-kind"));
    }

    @Test
    public void get() {
        PublicationMaker publicationMaker = registery.get("csv-authority-number-kind");
        assertTrue(publicationMaker.isMatching(" authority , number,kind  "));
        Publication publication = publicationMaker.create("CN,106008355,B");

        assertEquals("CN", publication.getAuthority());
        assertEquals("106008355", publication.getNumber());
        assertEquals("B", publication.getKind());
    }

    @Test
    public void getNotKnown() {
        try {
            registery.get("unknown");
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'name' with value 'unknown' is not a known PublicationMaker. Known names: [ csv-compact, csv-authority-number-kind ]", iae.getMessage());
        }
    }

    @Test
    public void getEmpty() {
        try {
            registery.get("  ");
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'name' should no be null.", iae.getMessage());
        }
    }

    @Test
    public void addDouble() {
        try {
            registery.add(makerCsvCompact);
            registery.add(makerCsvCompact);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'PublicationMaker' has name with value 'csv-compact', which has already been registered.", iae.getMessage());
        }
    }
}
