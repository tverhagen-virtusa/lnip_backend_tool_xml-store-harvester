package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PublicationMakerTest {
    // XML File
    private final static String PATTERN_1ST_LINE_XML = "^xml$";
    private final static String PATTERN_XML_FILE = Publication.PATTERN_PUBLICATION + "\\.xml";
    // CSV line: id, pid, change-date, update-date
    private final static String PATTERN_1ST_LINE_CSV_FULL = "^id,pid,change-date,update-date$"; 
    private final static String PATTERN_LINES_CSV_FULL = "(?<id>[0-9]+),\"?" + Publication.PATTERN_PUBLICATION + "\"?,(?<changedate>[0-9: ]+),(?<updatedate>[0-9: ]*)?"; 
    // CSV linet: id, pid
    private final static String PATTERN_1ST_LINE_CSV_COMPACT = "^id,pid$"; 
    private final static String PATTERN_CSV_COMPACT = "^(?<id>[0-9]+)," + Publication.PATTERN_PUBLICATION + "$";


    // XML File

    @Test
    public void createFromFile() {
        PublicationMaker publicationMaker = createPublicationMakerXmlFile();
        String filename = "JO2881B.xml";
        Publication pub = publicationMaker.create(filename);

        assertNotNull(pub);
        assertEquals("JO", pub.getAuthority());
        assertEquals("2881", pub.getNumber());
        assertEquals("B", pub.getKind());
    }

    private PublicationMaker createPublicationMakerXmlFile() {
        return PublicationMaker.createPublicationMaker("xml-file", PATTERN_1ST_LINE_XML, PATTERN_XML_FILE);
    }

    @Test
    public void createFromFileNotValid() {
        PublicationMaker publicationMaker = createPublicationMakerXmlFile();
        String filename = "incorrect-file-name.xml";
        try {
            publicationMaker.create(filename);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'text' with value 'incorrect-file-name.xml' is not a match, for the regular expression "
                    + "'" + PATTERN_XML_FILE + "'", iae.getMessage());
        }
    }

    @Test
    public void createFromFile1() throws Exception {
        PublicationMaker publicationMaker = createPublicationMakerXmlFile();
        String filename = "JO909B.xml";
        Publication pub = publicationMaker.create(filename);
        
        assertNotNull(pub);
        assertEquals("JO", pub.getAuthority());
        assertEquals("909", pub.getNumber());
        assertEquals("B", pub.getKind());
    }
    @Test
    public void createFromFile2() throws Exception {
        PublicationMaker publicationMaker = createPublicationMakerXmlFile();
        String filename = "US0000000B1.xml";
        Publication pub = publicationMaker.create(filename);

        assertNotNull(pub);
        assertEquals("US", pub.getAuthority());
        assertEquals("0000000", pub.getNumber());
        assertEquals("B1", pub.getKind());
        assertEquals("US0000000B1", pub.getPid());
    }

    // CSV Compact

    @Test
    public void createFromCompact() {
        PublicationMaker publicationMaker = createPublictionMakerCsvCompact();
        String line = "141866719,US10000035B2"; 
        Publication pub = publicationMaker.create(line);

        assertNotNull(pub);
        assertEquals("US", pub.getAuthority());
        assertEquals("10000035", pub.getNumber());
        assertEquals("B2", pub.getKind());
        assertEquals("US10000035B2", pub.getPid());
        assertEquals("US-10000035-B2", pub.toString());
    }

    private PublicationMaker createPublictionMakerCsvCompact() {
        return PublicationMaker.createPublicationMaker("csv_internal-id_publication-id", PATTERN_1ST_LINE_CSV_COMPACT, PATTERN_CSV_COMPACT);
    }

    @Test
    public void createFromCompactNotValid() {
        PublicationMaker publicationMaker = createPublictionMakerCsvCompact();
        String line = "1418619,US 10000035B2";
        try {
            publicationMaker.create(line);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'text' with value '1418619,US 10000035B2' is not a match, for the regular expression "
                    + "'" + PATTERN_CSV_COMPACT + "'", iae.getMessage());
        }
    }

    // CSV Full

    @Test
    public void createFromFull() {
        PublicationMaker publicationMaker = createPublicationMakerCsvFull();
        String firstLine = "id,pid,change-date,update-date";
        String line = "141866719,US10000035B2, 2015:01:02, 2019:10:20";
        assertTrue(publicationMaker.isMatching(firstLine));
        Publication pub = publicationMaker.create(line);

        assertNotNull(pub);
        assertEquals("US", pub.getAuthority());
        assertEquals("10000035", pub.getNumber());
        assertEquals("B2", pub.getKind());
        assertEquals("US10000035B2", pub.getPid());
        assertEquals("US-10000035-B2", pub.toString());
    }

    private PublicationMaker createPublicationMakerCsvFull() {
        return PublicationMaker.createPublicationMaker("csv_internal-id_publication-id_change-date_update-date", PATTERN_1ST_LINE_CSV_FULL, PATTERN_LINES_CSV_FULL);
    }

    @Test
    public void createFromFullNotValid() {
        PublicationMaker publicationMaker = createPublicationMakerCsvFull();
        String line = "141866719,US10000035B2, no-creation-date, no-update-date";
        try {
            publicationMaker.create(line);
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'text' with value '141866719,US10000035B2, no-creation-date, no-update-date' is not a match, for the regular expression "
                    + "'" + PATTERN_LINES_CSV_FULL + "'", iae.getMessage());
        }
    }

    
    private String asCsv_Id_Publication() {
        StringBuilder bldr = new StringBuilder();
        append(bldr, "Id,Pid");
        append(bldr, "141866719,US10000035B2");
        append(bldr, "141866986,US10000138B2");
        append(bldr, "141866833,US10000253B1");
        append(bldr, "42002824,US1000044A");
        return bldr.toString();
    }

    private String asCsv_id_publication_date() {
        StringBuilder bldr = new StringBuilder();
        append(bldr, "");
        append(bldr, "");
        return bldr.toString();
    }


    private void append(StringBuilder bldr, String text) {
        if (bldr.length() > 0) {
            bldr.append("\n");
        }
        bldr.append(text).append("\n");
    }

}
