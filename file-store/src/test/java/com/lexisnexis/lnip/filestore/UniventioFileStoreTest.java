package com.lexisnexis.lnip.filestore;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.lexisnexis.lnip.filestore.Publication;
import com.lexisnexis.lnip.filestore.UniventioFileStore;
import com.lexisnexis.lnip.filestore.config.UniventioFileStoreConfiguration;

public class UniventioFileStoreTest {
    private static final String XML = "xml";


    @Test(expected = IllegalArgumentException.class)
    public void createNull() throws IOException {
        new UniventioFileStore(null);
    }

    @Test
    public void create() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store-big";
        List<String> xmlStoreDirectories = Arrays.asList(new String[] {"xml-store-1", "xml-store-2", "xml-store-3", "xml-store-4", "xml-store-5", "xml-store-6"});

        UniventioFileStoreConfiguration cfg = UniventioFileStoreConfiguration.create(rootPathStr, xmlStoreDirectories, XML);
        UniventioFileStore xmlStore = new UniventioFileStore(cfg);
        
        Set<String> authorities = xmlStore.getAuthorities();
        assertEquals(11, authorities.size());

        assertEquals("src/test/resources/data/xml-store-big/xml-store-1", xmlStore.getPath("US").toString());
        assertEquals("src/test/resources/data/xml-store-big/xml-store-3", xmlStore.getPath("JP").toString());

        try {
            xmlStore.getPath("QQ").toString();
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'authority' with value 'QQ' is not a known authority.", iae.getMessage());
        }
        try {
            xmlStore.toPath(null, null);
            fail();
        }
        catch (IllegalArgumentException iae) {
            assertEquals("Argument 'publication' should not be null.", iae.getMessage());
        }
    }

    @Test
    public void getPublicationPath() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store";
        List<String> xmlStoreDirectories = Arrays.asList(new String[] {"s1", "s6"});

        UniventioFileStoreConfiguration cfg = UniventioFileStoreConfiguration.create(rootPathStr, xmlStoreDirectories, XML);
        UniventioFileStore xmlStore = new UniventioFileStore(cfg);
        
        Publication publication = Publication.create("US10000085B2");
        String expectedPath = "US/00/00/D1/US10000085B2.xml";
        String foundPathStr = xmlStore.toPath(publication, XML).toString();
        assertEquals(expectedPath, foundPathStr);

        expectedPath = "s1/" + expectedPath;
        foundPathStr = xmlStore.getPath(publication, XML).toString();
        assertEquals(expectedPath, foundPathStr.substring(foundPathStr.length() - expectedPath.length()));
    }
    

    @Test
    public void checkXmlStoreDirty() throws IOException {
        String rootPathStr = "src/test/resources/data/xml-store-dirty";
        List<String> xmlStoreDirectories = Arrays.asList(new String[] {"s1"});

        UniventioFileStoreConfiguration cfg = UniventioFileStoreConfiguration.create(rootPathStr, xmlStoreDirectories, XML);
        UniventioFileStore xmlStore = new UniventioFileStore(cfg);

        // The directory contains garbage, so only authority directories should be returned
        assertEquals(1, xmlStore.getAuthorities().size());
    }

    @Test
    public void createPath() throws IOException {
        String rootPathStr = "target/new-xml-store";
        Path rootPath = createCleanDirectory(rootPathStr);
        List<String> xmlStoreDirectories = Arrays.asList(new String[] {"s1/US", "s1/CY"});
        createDirectories(rootPath, xmlStoreDirectories);

        UniventioFileStoreConfiguration cfg = UniventioFileStoreConfiguration.create(rootPathStr, xmlStoreDirectories, XML);
        UniventioFileStore xmlStore = new UniventioFileStore(cfg);

        Publication.create("CY1118421T");
        xmlStore.createPath(Publication.create("US10000085B2"));
        xmlStore.createPath(Publication.create("CY1118421T"));
        xmlStore.createPath(Publication.create("CY1118421T"));
        xmlStore.createPath(Publication.create("CY1120524T"));

        assertTrue(Files.isDirectory(rootPath.resolve("s1/US/00/00/D1")));
        assertTrue(Files.isDirectory(rootPath.resolve("s1/CY/11/18/D1")));
        assertTrue(Files.isDirectory(rootPath.resolve("s1/CY/11/20/D2")));
    }

    private void createDirectories(Path rootPath, List<String> xmlStoreDirectories) {
        xmlStoreDirectories.stream()
                .forEach(p -> {
                    try {
                        Files.createDirectories(rootPath.resolve(p));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    private Path createCleanDirectory(String pathStr) throws IOException {
        return createCleanDirectory(Paths.get(pathStr));
    }

    private Path createCleanDirectory(Path path) throws IOException {
        removeDirectory(path);
        Files.createDirectories(path);
        return path;
    }
    private void removeDirectory(Path path) throws IOException {
        if (Files.exists(path)) {
            if (Files.isDirectory(path)) {
                Files.list(path).forEach(p -> {
                    try {
                        if (Files.isDirectory(p)) {
                            removeDirectory(p);
                        }
                        Files.delete(p);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            Files.delete(path);
        }
    }
}
