package com.lexisnexis.lnip.filestore.command;

import static com.lexisnexis.lnip.filestore.command.AppUtil.add;
import static org.junit.Assert.*;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import picocli.CommandLine;
import picocli.CommandLine.ParseResult;

public class UniventioFileStoreAppTest {

    @Test
    public void copy() throws Exception {
        List<String> commandLine = new ArrayList<>();
        add(commandLine, "copy");
//        add(commandLine, "--publication-maker", "csv-authority-number-kind");
//        add(commandLine, "--configuration ", "");
//        add(commandLine, "--destination", "target/json-store/output-sample");
//        add(commandLine, "src/test/resources/data/publication-id-comma.csv");


        assertEquals(Integer.valueOf(1), UniventioFileStoreApp.mainExecution(commandLine.toArray(new String[0])));
//        try {
//            fail();
//        }
//        catch (Exception e) {
//            assertEquals("123", e.getMessage());
//        }
    }

    @Test
    public void transform() throws Exception {
        List<String> commandLine = new ArrayList<>();
        add(commandLine, "transform");
//        add(commandLine, "--publication-maker", "csv-authority-number-kind");
//        add(commandLine, "--configuration ", "");
        add(commandLine, "--destination", "target/cmd-transformation");
        add(commandLine, "--transformation-name", "display-transformation");
        // TODO Input should be one or multiple xml files, or csv containing the publication-ids that need to be transformed
        add(commandLine, "src/test/resources/data/xml-store/s1/US/00/00/D1/US20170000136A1.xml");


        assertEquals(Integer.valueOf(0), UniventioFileStoreApp.mainExecution(commandLine.toArray(new String[0])));
        assertTrue(Files.isRegularFile(Paths.get("target/cmd-transformation").resolve("US20170000136A1.json")));
//        try {
//            fail();
//        }
//        catch (Exception e) {
//            assertEquals("123", e.getMessage());
//        }
    }
}
