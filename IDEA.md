

Add everything to git

Univentio File Store

- [ ] Add patent documents (xml, search-json, display-json) to UFS
- [ ] Get documents from UFS
- [ ] Update / Replace documents in UFS
- [ ] Remove documents from UFS
- [ ] Clone patent documents form one UFS to another UFS (checks if document exists and if so, if it is older, it will get copied)
      Use rsync
- [.] Transform XML to JSON
      - Place them in separate file-store, clone attributes (creation-time and update time) from original XML file
        `touch  -r  from-file  to-file`
        So it's easy to see, in case the original XML gets updated, some new json files are needed as well
- [ ] Chain UFSes
      - Normal set up:
        UFS App uses single UFS instance:
        App -> [Main Central UFS]
      - Two stages:
        UFS App uses two UFS instances:
        App -> [Local UFS] -> [Main Central UFS]
        It first uses the Local UFS, if the requested files are not found there, it uses the Main Central UFS.
- [ ] Proxy UFS
      - When looking up a file and it is not found in the Local UFS, get it from Main Central UFS. And place it in the Local UFS.
- [ ] Use `rsync` for creating daily snapshot of XML-Store
      - read (or generate) Univantio path structure from Main XML Store, then rsync each structure entry (like: DE/00/00)
- [ ] Distribute incoming files, over multiple storages entries:
      Similar as the authority entries are distributed over multiple entries, also output should be (flexible) distributed.
      DE        -> drive-A (default)
      DE/50..99 -> drive-B (entries 50 to 99 are stored on B)
      Check if all Authorities, have their entries <authority/[00..99]/[00..99]
      echo -e {00..99}"\n" | xargs -I % echo %|sed -e 's/^[0-9]$/0&/'

- [ ] restful interface, for adding and retrieving documents
      - get
      - add (post)
      - update (post)
      - remove (delete)

Usage

- Get XML documents out of central UFS
  - Use csv as input, to extract multiple files as once
  - Place them in identical Univentio path structure
  - Add translated types (xml -> json)
  - Create UniventioDocId Index (as file), for lookup by UniventioDocId, instead of PublicationId


- Create Univentio Directory Structure

    echo -e '['{9..10}']['{9..10}"]\n"|sed -e 's/\[\([0-9]\{1\}\)\]/[0\1]/g;s/\[/\//g;s/\]//g'|xargs -I % mkdir -p  ./tmp/DE%/D1 ./tmp/DE%/D2


Patent Document
  DE1234.xml
  DE1234-4321-search.json
  DE1234-4321-display.json
  Make an UniventioDocId to PublicationId file (per authority?)
  4321 <-> DE1234 | creation-timestamp | last-modified-timestamp
