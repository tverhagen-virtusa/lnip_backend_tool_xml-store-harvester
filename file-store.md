# File-Store 

From XML store to time-machine file store


## Background

- XML-Store randomly updates all the time documents
- For testing we need (relative) stable sets
- Cloning even half of all publication documents from XML-Store takes a lot of time 
  and disk space.
- Current xml-store contains around 130 million documents (10Tb disk space)


## The Idea

- Store huge amounts of files, like XML-Store does.
- Store them in such a way, that there is a daily stable set.
- Keep the stable per day sets for a certain amount of time 3 or 6 months (or whatever time makes sense)

*Extra features*

- Store not only xml document, but also the search-json and display-json, so indexing 
  elastic can be done a lot faster, as the conversion (currently done through import-manager) 
  takes considerable amount of time (per document loaded).
  https://lnipsolutions.atlassian.net/browse/SRCH-753
- Compress (zip) xml, it safe around 70% of disk space


## Pros and Cons

### Pros

- Time based storage of publication documents
- Faster filling of ES indexes
- Storing more data, on same disk space
- Instant access to stable set of documents. Recent history (last # months) of publications
  

### Cons

- As long as original XML Store exists double amount of disk space cost
- Multiple Servers XML store and File-Store


## Getting original XML documents in the new File-Store

There are multiple possibilities, how the new File-Store can get it's content.

- Do a daily rsync with option --link-dest
  The new server on which the File-Store is running would need ssh access for rsync 
  on the remote machine(s), for optimal performance. Accessing the shared network drives 
  is to slow.

- Each time a document is written to the current XML-Store, also sent that same document 
  to the new File-Store. Through a Restful API. Or just by placing them in a shared 
  directory. Which acts as a buffer, and than they are picked up and placed in the 
  proper location, by the new File-Store.


## Multiple ways how it can be set up in production

There is no real preference or preferred way. It just can start as a helper tool, for 
development and testing. But in a final situation, one could also think of a complete 
replacement for the current XML store.


1. File-Store for Dev and test
   Just use the File-Store, with it's json content, for quickly inject lots of publication 
   documents into a Elasticsearch cluster.
   There will be an API to do this. It will get a csv document, with the publication-ids 
   to upload and a configuration (or reference to a configuration), for which ES cluster 
   to upload to.
   - For speed this will be direct injection into ES cluster
   - For Import-Manager testing, it can be done through Import-Manager as well. Sending 
     it publication documents in XML format.
   - 

2. File-Store for Dev, Test and Prd
   
    
## Related Tasks

- https://lnipsolutions.atlassian.net/browse/SRCH-713


 

